FROM nginx:alpine
# Solution efficace mais "sale" car tout est dans le container
# COPY . /usr/share/nginx/html


# Solution plus élégante (Attention au dernier : il faut préciser le chemin complet)
COPY css /usr/share/nginx/html/css
COPY js /usr/share/nginx/html/js
COPY img /usr/share/nginx/html/img
COPY index.html /usr/share/nginx/html/


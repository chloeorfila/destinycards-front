# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import FirefoxOptions

class TestDestinycards():
  def setup_method(self, method):
    options = FirefoxOptions()
    options.add_argument("--headless")
    self.driver = webdriver.Firefox(firefox_binary="/usr/bin/firefox", options=options)
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_destinycards(self):
    self.driver.get("https://chloeorfila.gitlab.io/destinycards-front/staging/")
    time.sleep(30)
    self.driver.find_element(By.CSS_SELECTOR, ".default_card:nth-child(1) > .front").click()
    assert self.driver.find_element(By.CSS_SELECTOR, ".fliped > .back").text == " Those who know do not speak. Those who speak do not know. - Lao Tseu "
  
